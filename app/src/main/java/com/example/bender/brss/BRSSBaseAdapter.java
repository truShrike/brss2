package com.example.bender.brss;

/**
 * Created by Bender on 010 10.02.15.
 * Database Adapter
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * * Class for working with database
 */
public class BRSSBaseAdapter {

    /************Chanels Table Fields ************/
    public static final String KEY_CHANEL_NAME = "ChanelName";
    public static final String KEY_CHANEL_HTTP = "ChanelHTTP";
    public static final String KEY_CHANEL_DATE = "ChanelTIME";
    public static final String KEY_CHANEL_COUNT = "ChanelCOUNT";
    public static final String KEY_CHANEL_READ = "ChanelREAD";

    /************Chanels ITEMS Table Fields ************/
    public static final String KEY_CHANELITEMS_ID = "ChId";
    public static final String KEY_CHANELITEMS_TITLE = "ChTitle";
    public static final String KEY_CHANELITEMS_TIME = "ChTime";
    public static final String KEY_CHANELITEMS_TEXT = "ChText";
    public static final String KEY_CHANELITEMS_LINK = "ChLink";
    public static final String KEY_CHANELITEMS_READED = "ChR";
    public static final String KEY_CHANELITEMS_IMAGE = "ChImage";
    public static final String KEY_CHANELITEMS_AUTHOR = "Chauthor";

    /************* Database Name ************/
    public static final String DATABASE_NAME = "BRSS_DB_sqllite";

    /**** Database Version (Increase one if want to also upgrade your database) ****/
    public static final int DATABASE_VERSION = 1;// started at 1

    /** Table names */
    public static final String BRSS_CHANELS = "BrssChanels";
    public static final String BRSS_ITEMS = "BrssItems";
    /** Create table BrssChanels syntax */

    private static final String BRSS_CHANELS_CREATE = "create table "+BRSS_CHANELS +
            "( _id integer primary key autoincrement,"+
            KEY_CHANEL_NAME +" text," +
            KEY_CHANEL_HTTP +" text," +
            KEY_CHANEL_DATE +" text," +
            KEY_CHANEL_COUNT +" integer," +
            KEY_CHANEL_READ +" integer);";

    /** Create table BrssItems syntax */
    private static final String BRSS_ITEMS_CREATE = "create table "+BRSS_ITEMS +
            "( _id integer primary key autoincrement,"+
            KEY_CHANELITEMS_ID +" integer," +
            KEY_CHANELITEMS_TITLE +" text," +
            KEY_CHANELITEMS_TIME +" text," +
            KEY_CHANELITEMS_TEXT +" text," +
            KEY_CHANELITEMS_LINK +" text," +
            KEY_CHANELITEMS_READED +" integer,"+
            KEY_CHANELITEMS_IMAGE+  " text,"+
            KEY_CHANELITEMS_AUTHOR+" text);";


    private final Context mCtx;


    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    /**
     * BRSSBaseAdapter class constructor
     * @param ctx
     */



    /**  Add Record to Chanels table
     * @param Chname Chanel name
     * @param Chttp  Chanel link
     */

    public  void addChanel(String Chname,String Chttp) {

        ContentValues cVal = new ContentValues();
        cVal.put(KEY_CHANEL_NAME, Chname);
        cVal.put(KEY_CHANEL_HTTP, Chttp);
        cVal.put(KEY_CHANEL_DATE, "333");
        cVal.put(KEY_CHANEL_COUNT, 0);
        cVal.put(KEY_CHANEL_READ, 0);
        mDB.insert(BRSS_CHANELS, null, cVal);


    }

    /**
     *  Delete selected chanel
     * @param ChId
     */
    public  void delChanel(int ChId) {
        mDB.delete(BRSS_CHANELS,"_id = "+ChId,null);
    }

    /**
     * Delete all rss posts in selected chanel
     * @param ChId
     */
    public  void delChanelPosts(int ChId) {
        mDB.delete(BRSS_ITEMS,KEY_CHANELITEMS_ID+"="+ChId,null);
    }

    /**
     * Delete selected post
     * @param ChId
     */
    public  void delPostItem(int ChId) {
        mDB.delete(BRSS_ITEMS,"_id = "+ChId,null);
    }

   /**
    * Get data from Chanels
    *
    *  */
    public Cursor getData() {

            return mDB.query(BRSS_CHANELS, null, null, null, null, null, null);
    }

    /**
     * Get data from Chanels and information about number of posts in each chanel
     * @return cursor
     */
    public Cursor getChanelsData() {

        String selection;
        selection = "SELECT BrssChanels._id , BrssChanels.ChanelName, BrssChanels.ChanelHTTP,BrssChanels.ChanelTIME, COUNT(BrssItems._id),+" +
                     "COUNT( CASE WHEN BrssItems.Chr>0 THEN 1 ELSE NULL END) "+
                     "FROM BrssChanels LEFT  JOIN  BrssItems ON BrssItems.Chid= BrssChanels._id "+
                     "GROUP by BrssChanels.ChanelName";
        return mDB.rawQuery(selection,null);

    }

    /**
     *  Getting all posts from selected chanel
     * @param chanelId
     * @return Cursor
     */

    public Cursor getPosts(Integer chanelId) {

        return mDB.query(BRSS_ITEMS, null, KEY_CHANELITEMS_ID+"="+String.valueOf(chanelId), null, null, null, null);
    }

    public Cursor getRandomPost() {

        return mDB.rawQuery("SELECT * FROM BrssItems",null);
    }

    /**
     * Getting number of unreaded posts
     * @return
     */
    public Integer getNewCount(){
        Cursor c =mDB.query(BRSS_ITEMS, null,KEY_CHANELITEMS_READED+"='0'",null, null,null, null);
        int Count =c.getCount();
        c.close();
        return Count;
    }
    public Integer getChanelNewCount(Integer chanelId){
        Cursor c =mDB.query(BRSS_ITEMS, null,KEY_CHANELITEMS_READED+"='0' AND "+KEY_CHANELITEMS_ID+"="+String.valueOf(chanelId),null, null,null, null);
        int Count =c.getCount();
        c.close();
        return Count;
    }

    /**
     *
     * @param Title post title
     * @return 1 if post present in databasae,  otherwise
     */
    public Integer checkTitle(String Title) {

        String Title2=Title.replaceAll("'","''");
        Cursor c =mDB.rawQuery(" SELECT * FROM BrssItems WHERE ChTitle = '"+Title2+"'",null);
        int Count =c.getCount();
        c.close();
        if (Count==0 ) {
             return 0;
         }
        else {

             return 1;
        }

    }

    /**
     * Closing database
     */
    public void Close()
    {
        mDB.close();
    }

    /**
     * Change post status, if post was readed by reader
     * @param iPostId
     */
    public void setPostReaded(Integer iPostId)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_CHANELITEMS_READED,1);
        mDB.update(BRSS_ITEMS, cv,"_id=?",  new String[] { String.valueOf(iPostId) });
    }

    /**
     * set chanel update time
     * @param chanelId
     * @param sUpdateTime
     */
    public void setChanelpdateTime(Integer chanelId, String sUpdateTime)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_CHANEL_DATE,sUpdateTime);
        mDB.update(BRSS_CHANELS, cv,"_id=?",  new String[] { String.valueOf(chanelId) });
    }

    /**
     *  Adding rss post to database
     * @param chanelIdid
     * @param postTitle
     * @param postTime
     * @param postText
     * @param postLink
     * @param ImageLink
     * @param Author
     */
    public void addPostItem(Integer chanelIdid,String postTitle,String postTime,String postText,String postLink,String ImageLink,String Author)
    {
        mDB.beginTransaction();
        ContentValues cVal = new ContentValues();
        cVal.put(KEY_CHANELITEMS_ID, chanelIdid);
        cVal.put(KEY_CHANELITEMS_TITLE, postTitle);
        cVal.put(KEY_CHANELITEMS_TIME, postTime);
        cVal.put(KEY_CHANELITEMS_TEXT, postText);
        cVal.put(KEY_CHANELITEMS_LINK, postLink);
        cVal.put(KEY_CHANELITEMS_READED, 0);
        cVal.put(KEY_CHANELITEMS_IMAGE, ImageLink);
        cVal.put(KEY_CHANELITEMS_AUTHOR, Author);

        try {
            mDB.insert(BRSS_ITEMS, null, cVal);
            mDB.setTransactionSuccessful();
        } finally {
            mDB.endTransaction();
        }

    }

    public BRSSBaseAdapter(Context ctx) {
        mCtx = ctx;
        mDBHelper = new DBHelper(mCtx, DATABASE_NAME, null, DATABASE_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }


        @Override
        /**
         *  Create and fill table of database
         *  @param db - SQLiteDatabase
         */
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(BRSS_CHANELS_CREATE);
            db.execSQL(BRSS_ITEMS_CREATE);

            db.execSQL("INSERT INTO BrssChanels VALUES(1,'Украинская правда','http://www.pravda.com.ua/rus/rss/','date',0,0)");
            db.execSQL("INSERT INTO BrssChanels VALUES(2,'АНЕКДОТЫ','http://anekdotov.net/export/todayanekdots.rss','date',0,0)");
            db.execSQL("INSERT INTO BrssChanels VALUES(3,'CBC Top Stories','http://www.cbc.ca/cmlink/rss-topstories','date',0,0)");


        }
        /**
         * Update database
         * @param db - database to update
         * @param oldVersion - old version number
         * @param newVersion - new version number
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+ BRSS_CHANELS);
            db.execSQL("DROP TABLE IF EXISTS "+ BRSS_ITEMS);
            onCreate(db);
        }
    }
}
