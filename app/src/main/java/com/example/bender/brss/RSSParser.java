package com.example.bender.brss;





import java.util.*;
import java.net.*;
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;


/**
 * Created by Bender on 05.01.2015.
 * @author Alekxey Makarenko
 * Class for parsing RSS
 */
public class RSSParser {
    /**
     * String values for RSS root and elements
     */
    private final String RSS_ITEM="item";
    private final String RSS_TITLE="title";
    private final String RSS_DESCRIPTION="description";
    private final String RSS_LINK="link";
    private final String RSS_DATE="pubDate";
    private final String RSS_AUTHOR="author";
    /**
     * Transform RSS address string into URL, connect to server and get RSS stream
     * @param address - URL string
     * @return InputStream
     */

    /**
     * XML parser
     * @return List<PostItem>
     */
    public  ArrayList<PostItem> parse(String feedUrl) {

        final  ArrayList<PostItem> rssItems = new ArrayList<PostItem>();


        try {
            //open an URL connection make GET to the server and
            //take xml RSS data
            URL url = new URL(feedUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();


            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                //DocumentBuilderFactory, DocumentBuilder are used for
                //xml parsing
                DocumentBuilderFactory dbf = DocumentBuilderFactory
                        .newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();

                //using db (Document Builder) parse xml data and assign
                //it to Element
                Document document = db.parse(is);
                Element element = document.getDocumentElement();

                //take rss nodes to NodeList
                NodeList nodeList = element.getElementsByTagName(RSS_ITEM);

                if (nodeList.getLength() > 0) {
                    for (int i = 0; i < nodeList.getLength(); i++) {

                        //take each entry (corresponds to <item></item> tags in
                        //xml data

                        Element entry = (Element) nodeList.item(i);

                        Element _titleE = (Element) entry.getElementsByTagName(
                                RSS_TITLE).item(0);
                       Element _author = (Element) entry.getElementsByTagName(
                                RSS_AUTHOR).item(0);
                        String Res="222";
                        String PAuthhor="Author";
                        Element _descriptionE = (Element) entry
                              .getElementsByTagName(RSS_DESCRIPTION).item(0);
                        String Desc=_descriptionE.getTextContent();


                        if (Desc.indexOf("<img title=")>0) {
                            int indexS = Desc.indexOf("src='http://");
                            int indexE = Desc.indexOf(">");

                            Res = Desc.substring(indexS + 5, indexE - 3);
                        }

                        Element _pubDateE = (Element) entry
                                .getElementsByTagName(RSS_DATE).item(0);
                        Element _linkE = (Element) entry.getElementsByTagName(
                                RSS_LINK).item(0);
                        if (_author!=null) {
                            PAuthhor = _author.getFirstChild().getNodeValue();
                        }
                        PostItem rssItem = new PostItem();
                        rssItem.setTitle(_titleE.getFirstChild().getNodeValue());
                        rssItem.setDate(_pubDateE.getFirstChild().getNodeValue());
                        rssItem.setDescription(_descriptionE.getFirstChild().getNodeValue());
                        rssItem.setLink(_linkE.getFirstChild().getNodeValue());
                        rssItem.setCbcImageText(Res);
                        rssItem.setAuthor( PAuthhor);
                        rssItems.add(rssItem.copy());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rssItems;
    }















}
