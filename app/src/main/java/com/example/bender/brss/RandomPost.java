package com.example.bender.brss;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.Calendar;
import java.util.Random;


/**
 * Implementation of App Widget functionality.
 */
public class RandomPost extends AppWidgetProvider {

    BRSSBaseAdapter BRSSDB;
    Cursor cursor;
    public static String RANDOM_WIDGET_UPDATE = "RANDOM_WIDGET_UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int id : appWidgetIds) {

            updateAppWidget(context, appWidgetManager, id);

        }
    }


    @Override
    public void onEnabled(Context context) {
        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        final Calendar TIME = Calendar.getInstance();
        TIME.set(Calendar.MINUTE, 0);
        TIME.set(Calendar.SECOND, 0);
        TIME.set(Calendar.MILLISECOND, 0);

        final Intent intent = new Intent(RANDOM_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        //  Toast.makeText(context,"Alarm Start", Toast.LENGTH_SHORT).show();
        m.setRepeating(AlarmManager.RTC_WAKEUP, TIME.getTime().getTime(), 1000 * 60, pendingIntent);
    }

    @Override
    public void onDisabled(Context context) {

        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        final Intent intent = new Intent(RANDOM_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        m.cancel(pendingIntent);
    }

    @Override

    public void onReceive( Context context, Intent intent) {

        super.onReceive(context, intent);


        if(RANDOM_WIDGET_UPDATE.equals(intent.getAction())){
            // Toast.makeText(context, "Total Receive "+intent.getAction(), Toast.LENGTH_SHORT).show();
            Bundle extras = intent.getExtras();
            if(extras!=null) {

                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                ComponentName thisAppWidget = new ComponentName(context.getPackageName(), RandomPost.class.getName());
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

                onUpdate(context, appWidgetManager, appWidgetIds);
            }
        }
    }

    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        BRSSDB = new BRSSBaseAdapter(context);
        cursor=BRSSDB.getRandomPost();
        Random r = new Random();
        cursor.moveToPosition(r.nextInt(cursor.getCount()));
        RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.random_post);
        updateViews.setTextViewText(R.id.appwidget_chanelTitle, cursor.getString(2));
        updateViews.setTextViewText(R.id.appwidget_chaneText, cursor.getString(4));
        appWidgetManager.updateAppWidget(appWidgetId, updateViews);
    }
}


