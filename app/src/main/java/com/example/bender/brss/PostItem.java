package com.example.bender.brss;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Bender on 05.01.2015.
 * @author Bender Makarenko
 * Class of RSS items
 */
public class PostItem implements Parcelable {
    /*String for data format*/
    static SimpleDateFormat FORMATTER = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.US);
    /*Post Title*/
    private String title;
    /*Post URL */
    private URL link;
    /*String value of URL*/
    private String linkText;
    /*Post description*/
    private String description;
    /*Post date*/
    private Date date;
    /*Post date*/
    private URL CbcImage;
    /*Post Image Link*/
    private String CbcImageText;
    /*Post Image Author*/
    private String Author;

    public void setAuthor(String author) {
        this.Author =author;
    }

    public String getAuthor() {
        return Author;
    }
    public void setCbcImageText(String Text) {
        this.CbcImageText = Text.trim();
    }
    public String getCbcImageText() {
        return CbcImageText;
    }
    /**
     * Setter for title
     * @param title
     */
    public void setTitle(String title) {
        this.title = title.trim();
    }
    /**
     * Getter for title
     * @return title
     */
    public String getTitle() {
        return title;
    }
    /**
     * Setter for description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description.trim();
    }
    /**
     * Getter for description
     * @return
     */
    public String getDescription() {
        return description;
    }
    /**
     * Setter for link and linkText
     * @param link
     */
    public void setLink(String link) {
        this.linkText = link.trim();
        try {
            this.link = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setCbcImage(String link) {
        this.CbcImageText = link.trim();
        try {
            this.CbcImage = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * Getter for link
     * @return
     */
    public URL getLink() {
        return link;
    }
    public URL getCbcImage() {
        return CbcImage;
    }
    /**
     * Getter for linkText
     * @return
     */
    public String getLinkText() {
        return linkText;
    }
    /**
     * Setter for date
     * @param date
     */
    public void setDate(String date) {
        try {
            this.date = FORMATTER.parse(date.trim());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * Getter для date
     * @return
     */
    public String getDate() {
        return FORMATTER.format(this.date);
    }
    /*Create and return new instance of PostItem filled with current object
    * @return copy
    */
    public PostItem copy() {
        PostItem copy = new PostItem();
        copy.title = title;
        copy.link = link;
        copy.linkText = linkText;
        copy.description = description;
        copy.date = date;
        copy.CbcImageText = CbcImageText;
        copy.Author = Author;
        return copy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(title);
        out.writeString(linkText);
        out.writeString(description);
        out.writeString(getDate());
    }
    public static final Creator<PostItem> CREATOR = new Creator<PostItem>() {
        public PostItem createFromParcel(Parcel source) {
            return new PostItem(source);
        }
        public PostItem[] newArray(int size) {
            return new PostItem[size];
        }
    };
    public PostItem(){}
    private PostItem(Parcel source) {
        title = source.readString();
        setDate(source.readString());
        linkText = source.readString();
        description = source.readString();
    }
}

