package com.example.bender.brss;

/**
 * Describe post item
 * Created by Bender on 019 19.02.15.
 */
public class BrssPostItem {

    int _id;
    Integer Id;
    Integer ChId;
    String Title;
    String Time;
    String Text;
    String Link;
    Integer Readed;
    String ImageLink;
    String Author;

    // constructors

    public BrssPostItem(Integer Id,Integer ChId, String Title,String Time,String Text,String Link, Integer Readed,String ImageLink,String Author ) {
        this.Id = Id;
        this.ChId = ChId;
        this.Title = Title;
        this.Time = Time;
        this.Text = Text;
        this.Link = Link;
        this.Readed = Readed;
        this.ImageLink = ImageLink;
        this.Author = Author;

    }
}
