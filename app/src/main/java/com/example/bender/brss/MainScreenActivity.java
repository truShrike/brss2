﻿package com.example.bender.brss;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;


/**
 * Main screen activity with list of all rss chanels
 */

// Test git
public class MainScreenActivity extends Activity {

    ListView listViewChanels;
    View chanelDialog;
    EditText inputName;
    EditText inputHttp;

    private static final int CM_DELETE_ID = 1;
    private static final int CM_CLEAR_ID = 2;
    private static final String CHANEL_ID = "ID";
    private static final String CHANEL_NAME = "Name";

    private static final int UPDATE_SECONDS = 60;

    BRSSBaseAdapter brssDabase;
    Cursor cChanels;
    BRSSChanelsListAdapter brssCLAdapter;
    BroadcastReceiver broadcastReceiver;
    public final static String BROADCAST_ACTION = "brss";

    /**
     * Show Selected chanel
     * @param chanelId  chanel id
     * @param name chanel name
     */
    public void ShowChanel(Integer chanelId, String name )
    {
        Intent intentIM = new Intent(this , ChanelActivity.class);
        intentIM.putExtra(CHANEL_ID, chanelId);
        intentIM.putExtra(CHANEL_NAME, name);
        startActivity(intentIM);

    }

    /**
     * refresh ListView
     */
   protected void refreshChanels() {

       cChanels = brssDabase.getChanelsData();
       brssCLAdapter = new BRSSChanelsListAdapter(this, cChanels);
       listViewChanels.setAdapter(brssCLAdapter);

   }


    @Override
    protected void onResume()
    {
        super.onResume();
        refreshChanels();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);


        brssDabase = new BRSSBaseAdapter(this);
        cChanels = brssDabase.getChanelsData();
        listViewChanels = (ListView) findViewById(R.id.listViewChanels);
        brssCLAdapter = new BRSSChanelsListAdapter(this, cChanels);
        listViewChanels.setAdapter(brssCLAdapter);
        registerForContextMenu(listViewChanels);

        listViewChanels.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ShowChanel((int) brssCLAdapter.getItemId(view.getId()),brssCLAdapter.getItem(view.getId()).chanelName);


            }
        });

        broadcastReceiver = new BroadcastReceiver(){
            public void onReceive(Context context, Intent intent) {

                refreshChanels();
            }

        };
        // Registering broadcast receiver and starting alarm manager
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, intFilt);
        //startService(new Intent(this, GetRssService.class));
        Intent SerIntent = new Intent(this, GetRssService.class);
        PendingIntent pintent = PendingIntent.getService(this, 0, SerIntent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),UPDATE_SECONDS*1000, pintent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, "Удалить");
        menu.add(0, CM_CLEAR_ID, 0, "Очистить");
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {

            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            brssDabase.delChanel((int)brssCLAdapter.getItemId(acmi.position));
            refreshChanels();

            return true;
        }
        if (item.getItemId() == CM_CLEAR_ID) {

            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            brssDabase.delChanelPosts((int) brssCLAdapter.getItemId(acmi.position));
            refreshChanels();

            return true;
        }

        return super.onContextItemSelected(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.addchanel) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Новая Лента");
            chanelDialog = getLayoutInflater().inflate(R.layout.add_chanel_dialog, null);
            alert.setView(chanelDialog);

            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    inputName = (EditText) chanelDialog.findViewById(R.id.editTextChName);
                    inputHttp = (EditText) chanelDialog.findViewById(R.id.editTextChHttp);
                    String value = inputName.getText().toString();
                    String value2 = inputHttp.getText().toString();

                    brssDabase.addChanel(value,value2);
                    cChanels =brssDabase.getChanelsData();
                    refreshChanels();


                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });
            alert.show();

        }
        if (id == R.id.updatescr) {
            startService(new Intent(this, GetRssService.class));
        }


        return super.onOptionsItemSelected(item);
    }
}