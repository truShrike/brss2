package com.example.bender.brss;

/**
 * Describe Chanel item
 * Created by Bender on 011 11.02.15.
 */
public class BrssChanel {

    int _id;
    Integer id;
    String chanelName;
    String chanelHTTP;
    String chanelTime;
    String chanelCount;
    String chanelRead;

    // constructors
    public BrssChanel() {
    }
    public BrssChanel(Integer Id,String ChanelName, String ChanelHTTP,String ChanelTime,String ChanelCount,String ChanelRead) {
        this.id = Id;
        this.chanelName = ChanelName;
        this.chanelHTTP = ChanelHTTP;
        this.chanelTime = ChanelTime;
        this.chanelCount = ChanelCount;
        this.chanelRead = ChanelRead;

    }
}
