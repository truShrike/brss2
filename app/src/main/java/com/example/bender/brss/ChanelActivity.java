package com.example.bender.brss;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Activity to show all posts in selected chanel
 */

public class ChanelActivity extends Activity {


    BRSSBaseAdapter brssDatabase; // Database
    Cursor cChanelItems;
    Integer chiD;
    String sChanelName;
    ListView listViewChanel;
    BRSSPostListAdapter postListAdapter;
    private static final int CM_DELETE_POST = 1;
    private static final String POST_LINK = "Link";
    private static final String POST_TEXT = "Text";
    private static final String POST_DATE = "Date";
    private static final String POST_TITLE = "Title";
    private static final String POST_NAME = "ChName";
    private static final String POST_IMAGE = "ImageLink";

    /**
     * Show activity wist selected post
     * @param Id
     */
    public void showPost(Integer Id )
    {
        Intent intentIM = new Intent(this , PostScreenActivity.class);
        BrssPostItem postItem;
        postItem = postListAdapter.getItem(Id);
        intentIM.putExtra(POST_LINK, postItem.Link);
        intentIM.putExtra(POST_TEXT, postItem.Text);
        intentIM.putExtra(POST_DATE, postItem.Time);
        intentIM.putExtra(POST_TITLE,postItem.Title);
        intentIM.putExtra(POST_NAME, sChanelName);
        intentIM.putExtra(POST_IMAGE, postItem.ImageLink);
        brssDatabase.setPostReaded(postListAdapter.getItem(Id).Id);
        startActivity(intentIM);

    }

    /**
     * refresh listview
     */
    protected void refreshItems() {

        cChanelItems = brssDatabase.getPosts(chiD);
        postListAdapter = new BRSSPostListAdapter(this, cChanelItems);
        listViewChanel.setAdapter(postListAdapter);

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        refreshItems();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanel_screenb);
        Intent intent = getIntent();
        chiD = intent.getIntExtra("ID",0);
        sChanelName = intent.getStringExtra("Name");
        setTitle(sChanelName);
        brssDatabase = new BRSSBaseAdapter(this);
        cChanelItems = brssDatabase.getPosts(chiD);

        listViewChanel = (ListView) findViewById(R.id.listViewChanel);
        postListAdapter =new BRSSPostListAdapter(this,cChanelItems);
        listViewChanel.setAdapter(postListAdapter);
        registerForContextMenu(listViewChanel);
        listViewChanel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                showPost(view.getId());
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chanel, menu);
        return true;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_POST, 0, "Удалить");
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_POST) {

            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Toast.makeText(getApplicationContext(),
                    String.valueOf((int)postListAdapter.getItemId(acmi.position)), Toast.LENGTH_LONG)
                    .show();
            brssDatabase.delPostItem((int)postListAdapter.getItemId(acmi.position));
            refreshItems();

            return true;
        }
        return super.onContextItemSelected(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
