package com.example.bender.brss;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

/**
 * Activity for configuration  Selected Chanel Counter widget
 */

public class ChanelCounterWidgetConfigActivity extends Activity {



    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    BRSSChanelsListAdapter brssCLAdapter;
    Intent resultValue;
    BRSSBaseAdapter brssDatabase;
    Cursor cChanels;
    ListView listViewChanels;
    public final static String WIDGET_PREF = "widget_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanel_counter_widget_config);
        setResult(RESULT_CANCELED);
        setTitle("Настройка виджета");
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If they gave us an intent without the widget id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
        // forming answer intent
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        // cancel answer
        setResult(RESULT_CANCELED, resultValue);
        brssDatabase = new BRSSBaseAdapter(this);
        cChanels = brssDatabase.getChanelsData();// Getting chanel from database
        listViewChanels = (ListView) findViewById(R.id.ChanelSelect);
        brssCLAdapter = new BRSSChanelsListAdapter(this, cChanels);
        listViewChanels.setAdapter(brssCLAdapter); // setting adapter

        listViewChanels.setOnItemClickListener(new AdapterView.OnItemClickListener() { // Listener to choose chanel
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //Toast.makeText(context, "HelloWidgetConfig.onClick(): " + String.valueOf(mAppWidgetId), Toast.LENGTH_LONG).show();
                SharedPreferences sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("CHANEL_ID" + mAppWidgetId, (int)brssCLAdapter.getItemId(view.getId()));
                editor.putString("CHANEL_NAME" + mAppWidgetId, brssCLAdapter.getItem(view.getId()).chanelName);
                editor.commit();

                resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                setResult(RESULT_OK, resultValue);
                finish();


            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chanel_counter_widget_config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
