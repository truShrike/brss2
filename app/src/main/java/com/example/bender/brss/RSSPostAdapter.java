package com.example.bender.brss;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Bender on 13.01.2015.
 * @author Bender
 * Adapter for custom listview with rss posts
 */
public class RSSPostAdapter extends BaseAdapter {
    /**
     * RSS items
     */
    private List<PostItem> items;
    /**
     * Layout Inflater for custom listview
     */
    LayoutInflater inflater;
    /**
     * Post text
     */
    TextView postText;

    /**
     * Activity context
     */
    Activity ctx;
    /**
     *
     * @param context -activity context
     * @param data - post items
     */
    public RSSPostAdapter(Activity context, List<PostItem> data){
        ctx=context;
        items=data;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        if (items!=null)
            return items.size();
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row, parent, false);
        TextView pubDate = (TextView) rowView.findViewById(R.id.pub_date);
        TextView postTitle = (TextView) rowView.findViewById(R.id.post_title);
        postText = (TextView) rowView.findViewById(R.id.post_text);
        pubDate.setText(items.get(i).getDate());
        postTitle.setText(items.get(i).getTitle());
        postText.setText(items.get(i).getDescription());
        postText.setTag(i);

        return rowView;
    }
}
