package com.example.bender.brss;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViews;

import java.util.Date;

public class UpdateWidgetsService extends Service {

    BRSSBaseAdapter BRSSDB;
    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        buildUpdate();

        return super.onStartCommand(intent, flags, startId);
    }

    private void buildUpdate()
    {
        BRSSDB = new BRSSBaseAdapter(this);
        RemoteViews view = new RemoteViews(getPackageName(), R.layout.total_counter_widget);
        view.setTextViewText(R.id.appwidget_text,String.valueOf( BRSSDB.getNewCount()));

        RemoteViews view2 = new RemoteViews(getPackageName(), R.layout.selected_chanel_counter);
        view2.setTextViewText(R.id.appwidget_text,"33");
        BRSSDB.Close();
        ComponentName thisWidget = new ComponentName(this, TotalCounterWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(thisWidget, view);
        thisWidget = new ComponentName(this, SelectedChanelCounter.class);
        manager.updateAppWidget(thisWidget, view2);
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}
