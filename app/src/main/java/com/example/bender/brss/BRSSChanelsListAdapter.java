package com.example.bender.brss;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adaper to listview on mainscreen
 * Created by Bender on 011 11.02.15.\
 */
public class BRSSChanelsListAdapter extends BaseAdapter  {

    public  ArrayList<BrssChanel> chanelList;
    private  Context _context;
    private TextView tvChanelName;
    private TextView tvChanelStatus;

    public BRSSChanelsListAdapter(Context context, Cursor Cur )
    {
        super();
        this._context = context;
        this.chanelList = new ArrayList<BrssChanel>();
        chanelList.clear();
        if (Cur.moveToFirst())
        {
            do {
                BrssChanel jAdd=new BrssChanel(Cur.getInt(0),Cur.getString(1),Cur.getString(2),Cur.getString(3),Cur.getString(4),Cur.getString(5) );
                chanelList.add(jAdd);
            } while (Cur.moveToNext());
        }


    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        if (row == null)
        {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.main_list_item, parent, false);
        }
        BrssChanel dvalue = getItem(position);
        tvChanelName =(TextView) row.findViewById(R.id.chanelName);
        tvChanelStatus =(TextView) row.findViewById(R.id.chanelStatus);
        tvChanelName.setText(dvalue.chanelName);
        tvChanelStatus.setText("Обновлено: "+dvalue.chanelTime+", "+dvalue.chanelRead+"/"+dvalue.chanelCount+" новых");

        row.setId(position);
        return row;
    }
    @Override
    public int getCount()
    {
        return chanelList.size();
    }
    @Override
    public long getItemId(int position)
    {
        return chanelList.get(position).id;
    }

    public BrssChanel getItem(int position)
    {
        return chanelList.get(position);
    }

}
