package com.example.bender.brss;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

public class PostScreenActivity extends Activity {


    String Link;
    String Text;
    String Date;
    String Title;
    String ChName;
    Integer iPostId;
    String ImageLink;
    TextView tvPostText;
    TextView tvHeader;
    Button bBrowserShow;
    ImageView postImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_screen);
        Intent intent = getIntent();
        Link = intent.getStringExtra("Link");
        Text = intent.getStringExtra("Text");
        Date = intent.getStringExtra("Date");
        Title = intent.getStringExtra("Title");
        ChName = intent.getStringExtra("ChName");
        iPostId = intent.getIntExtra("Itemid",0);
        ImageLink = intent.getStringExtra("ImageLink");
        setTitle(Title);

        postImage = (ImageView)findViewById(R.id.Postimage);
        if (ImageLink!="222") {

            Picasso.with(this).load(ImageLink).into(postImage);
        }
        tvPostText = (TextView) findViewById(R.id.PiText);
        tvPostText.setText(Text);
        tvHeader = (TextView) findViewById(R.id.Header);
        tvHeader.setText(Date+" "+ChName);
        bBrowserShow = (Button) findViewById(R.id.browsersow);

        bBrowserShow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Link)));

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_screen, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.Share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,Title+" "+Date+"\n"+Text+"\n"+Link);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Использовать:"));
        }

        return super.onOptionsItemSelected(item);
    }
}
