package com.example.bender.brss;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.RemoteViews;


import java.util.Calendar;


/**
 * Implementation of App Widget functionality.
 * Widget which shows counter of new posts in selected chanel
 */
public class SelectedChanelCounter extends AppWidgetProvider {
    BRSSBaseAdapter brssDabase;
    private static final int UPDATE_SECONDS = 60;
    public static String SELECTED_CH_WIDGET_UPDATE = "SELECTED_CH_WIDGET_UPDATE";

    @Override

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        SharedPreferences sp = context.getSharedPreferences(
                ChanelCounterWidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
        for (int id : appWidgetIds) {

            updateAppWidget(context, appWidgetManager,sp, id);

        }
    }

    @Override
    public void onReceive( Context context, Intent intent) {

        super.onReceive(context, intent);

        if(SELECTED_CH_WIDGET_UPDATE.equals(intent.getAction())){
            Bundle extras = intent.getExtras();
            if(extras!=null) {
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                ComponentName thisAppWidget = new ComponentName(context.getPackageName(), SelectedChanelCounter.class.getName());
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

                onUpdate(context, appWidgetManager, appWidgetIds);
            }
        }
    }


    @Override
    public  void  onDeleted (Context context,int[] appWidgetIds)
    {
        for (int id : appWidgetIds) {


            final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            final Intent intent = new Intent(SELECTED_CH_WIDGET_UPDATE);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);
            m.cancel(pendingIntent);
        }
    }
    @Override
    public void onDisabled(Context context) {


        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(SELECTED_CH_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        m.cancel(pendingIntent);
    }

    @Override
    public void onEnabled(Context context) {

        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        final Calendar TIME = Calendar.getInstance();
        TIME.set(Calendar.MINUTE, 0);
        TIME.set(Calendar.SECOND, 0);
        TIME.set(Calendar.MILLISECOND, 0);

        final Intent intent = new Intent(SELECTED_CH_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);


        m.setRepeating(AlarmManager.RTC_WAKEUP, TIME.getTime().getTime(), 1000 * UPDATE_SECONDS, pendingIntent);
    }


    public  void updateAppWidget(Context context, AppWidgetManager appWidgetManager, SharedPreferences sp,
                                 int appWidgetId){

        Integer chId = sp.getInt("CHANEL_ID" + appWidgetId, 0);
        String chanelName = sp.getString("CHANEL_NAME" + appWidgetId, null);
        brssDabase = new BRSSBaseAdapter(context);

        RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.selected_chanel_counter);

        updateViews.setTextViewText(R.id.appwidget_SelChanelName, chanelName);
        updateViews.setTextViewText(R.id.appwidget_SelChanelCount, String.valueOf( brssDabase.getChanelNewCount(chId)));
        appWidgetManager.updateAppWidget(appWidgetId, updateViews);

       // Toast.makeText(context, "updateAppWidget(): " + String.valueOf(appWidgetId) + "\n" + "Yohoho", Toast.LENGTH_SHORT).show();

    }
}


