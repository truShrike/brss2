package com.example.bender.brss;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.IBinder;
import android.widget.RemoteViews;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Bender on 05.01.2015.
 * @author Alekxey Makarenko
 * Class for parsing RSS
 */

/**
 * Service for getting data
 */
public class GetRssService extends Service  {

    BRSSBaseAdapter mbrssDatabase;
    Cursor mChanels;
    Integer mNewCount;
    Integer iCountThreadDone;
    NotificationManager Notifications;
    RemoteViews view;


    public GetRssService() {
    }

    /**
     * Sendind nortifications to action bar
     * @param mNewCount New posts counter
     */
    void sendNotif(Integer mNewCount) {

        Notification notif = new Notification(R.drawable.rsshippo, "Новых постов "+String.valueOf(mNewCount),
        System.currentTimeMillis());
        notif.ledARGB = Color.GREEN;
        notif.ledOnMS = 1000;
        notif.ledOffMS = 300;
        Intent intent = new Intent(this, MainScreenActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notif.number=mNewCount;
        notif.setLatestEventInfo(this, "Ленты RSS","Новых постов "+String.valueOf(mNewCount), pIntent);
        notif.defaults = Notification.FLAG_AUTO_CANCEL;
        //notif.flags = notif.flags| Notification.FLAG_SHOW_LIGHTS;
        Notifications.notify(1, notif);
    }

    public void onCreate() {
        super.onCreate();
        Notifications = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      //  Log.d(LOG_TAG, "onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
       // Log.d(LOG_TAG, "onDestroy");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(LOG_TAG, "onStartCommand");
        GetData();

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     *  Getting data from each rss chanel registered in database
     */
    void GetData() {
        mbrssDatabase = new BRSSBaseAdapter(this);
        mChanels = mbrssDatabase.getData();
        iCountThreadDone =0;
        mNewCount = 0;
        view = new RemoteViews(getPackageName(), R.layout.total_counter_widget);


        if (mChanels.getCount()>0) {  // checking the chanels  quantity

            mChanels.moveToFirst();
            Integer chanelId;
            String url;
            do{
                chanelId=mChanels.getInt(0); // getting chanel id
                url=mChanels.getString(2); // getting chanel url
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
                String currentDateandTime = sdf.format(new Date());
                mbrssDatabase.setChanelpdateTime(mChanels.getInt(0),currentDateandTime); // setting chanel update time
                ParseThread parseThread = new ParseThread();

                parseThread.setOnTaskFinishedEvent(new ParseThread.OnTaskExecutionFinished(){
                    public void OnTaskFihishedEvent(Integer result) {
                        iCountThreadDone++;
                            mNewCount+=result;
                                if (iCountThreadDone==mChanels.getCount()) { // parsing all chanels finished
                                    if (mNewCount>0) { // if present new posts, sending notifications
                                        sendNotif(mNewCount);
                                        Intent intent = new Intent(MainScreenActivity.BROADCAST_ACTION);
                                        sendBroadcast(intent);

                                    }

                                }
                            }

                        });
                        parseThread.setContext(this);
                        parseThread.execute(url, String.valueOf(chanelId));


            } while (mChanels.moveToNext());
            mChanels.close();
            mbrssDatabase.Close();
        }

    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
