package com.example.bender.brss;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.Calendar;

/**
 * Implementation of App Widget functionality.
 * Widget which shows total count of unreaded news
 */
public class TotalCounterWidget extends AppWidgetProvider {

    BRSSBaseAdapter brssDatabase;

    public static String MY_WIDGET_UPDATE = "MY_OWN_WIDGET_UPDATE";
    private static final int UPDATE_SECONDS = 60;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int id : appWidgetIds) {

            updateAppWidget(context, appWidgetManager, id);

        }
    }




   @Override

    public void onReceive( Context context, Intent intent) {

        super.onReceive(context, intent);


       if(MY_WIDGET_UPDATE.equals(intent.getAction())){

           Bundle extras = intent.getExtras();
          if(extras!=null) {  // Toast.makeText(context, "Total Receive "+intent.getAction(), Toast.LENGTH_SHORT).show();
               AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
               ComponentName thisAppWidget = new ComponentName(context.getPackageName(), TotalCounterWidget.class.getName());
               int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

               onUpdate(context, appWidgetManager, appWidgetIds);
           }
       }
    }

    @Override
    public void onEnabled(Context context) {


         final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        final Calendar TIME = Calendar.getInstance();
        TIME.set(Calendar.MINUTE, 0);
        TIME.set(Calendar.SECOND, 0);
        TIME.set(Calendar.MILLISECOND, 0);

        final Intent intent = new Intent(MY_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

      //  Toast.makeText(context,"Alarm Start", Toast.LENGTH_SHORT).show();
        m.setRepeating(AlarmManager.RTC_WAKEUP, TIME.getTime().getTime(), 1000 * UPDATE_SECONDS, pendingIntent);

    }

    @Override
    public void onDisabled(Context context) {


        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        final Intent intent = new Intent(MY_WIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        m.cancel(pendingIntent);
    }

    @Override
    public  void  onDeleted (Context context,int[] appWidgetIds)
    {


    }

    public  void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                       int appWidgetId){
        brssDatabase = new BRSSBaseAdapter(context);
        brssDatabase.getNewCount();
        RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.total_counter_widget);
        updateViews.setTextViewText(R.id.appwidget_text, String.valueOf( brssDatabase.getNewCount()));
        appWidgetManager.updateAppWidget(appWidgetId, updateViews);

      //  Toast.makeText(context, "updateAppWidget(): " + String.valueOf(appWidgetId) + "\n" + "Yohoho", Toast.LENGTH_SHORT).show();

    }

}


