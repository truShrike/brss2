package com.example.bender.brss;


import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bender on 05.01.2015.
 * Patrsing RSS thread
 */
public class ParseThread extends AsyncTask<String, Integer, ArrayList<PostItem>> {


    private RSSParser parser;
    private List<PostItem> messages;
    Context ctx;
    public Integer chanelId;
    BRSSBaseAdapter brssDatabase;
    String url;
    Integer newCounter;

    private OnTaskExecutionFinished _task_finished_event;

    public interface OnTaskExecutionFinished
    {
        public void OnTaskFihishedEvent(Integer Result);
    }

    public void setOnTaskFinishedEvent(OnTaskExecutionFinished _event)
    {
        if(_event != null)
        {
            this._task_finished_event = _event;
        }
    }


    public void setContext(Context Ctx){
        ctx=Ctx;
    }
    protected void onPreExecute() {
        parser = new RSSParser();

    }




    @Override
    protected ArrayList<PostItem> doInBackground(String... arg0) {
        chanelId = Integer.parseInt(arg0[1]);
        url = arg0[0];
        return parser.parse(arg0[0]);

    }


    /**
     * Adding posts posts to database
     * @param result - URL string ArrayList filled PostItem(s)
     * @return InputStream
     */

    protected void onPostExecute(ArrayList<PostItem> result) {
        if (result != null) {

            messages = result;
            newCounter = 0;
            brssDatabase = new BRSSBaseAdapter(ctx);


            for (PostItem msg : messages){
               if ( brssDatabase.checkTitle(msg.getTitle())==0) {// presence checking in the database
                    newCounter++;
                   brssDatabase.addPostItem(chanelId, msg.getTitle(), msg.getDate(), msg.getDescription(), msg.getLink().toString(),msg.getCbcImageText(),msg.getAuthor());
                }

            }

            brssDatabase.Close();

        }
        super.onPostExecute(result);
        if(this._task_finished_event != null)
        {
            this._task_finished_event.OnTaskFihishedEvent(newCounter);
        }
        else
        {
            //Log.d("SomeClass", "task_finished even is null");
        }
    }
}
