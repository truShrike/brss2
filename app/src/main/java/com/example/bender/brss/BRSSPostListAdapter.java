package com.example.bender.brss;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import com.squareup.picasso.Picasso;
/**
 * Created by Bender on 019 19.02.15.
 * Adapter to ListView on the chanel activity
 */
public class BRSSPostListAdapter extends BaseAdapter {

    public final ArrayList<BrssPostItem> list;
    private final Context _context;
    String imageSrc="NoImage";



    public BRSSPostListAdapter(Context context, Cursor Cur )
    {
        super();
        this._context = context;
        this.list = new ArrayList<BrssPostItem>();
        list.clear();
        if (Cur.moveToFirst())
        {
            do {
                BrssPostItem jAdd=new BrssPostItem(Cur.getInt(0),Cur.getInt(1),Cur.getString(2),Cur.getString(3),Cur.getString(4),
                                            Cur.getString(5),Cur.getInt(6),Cur.getString(7),Cur.getString(8) );
                list.add(jAdd);
            } while (Cur.moveToNext());
        }


    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;

         LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         row = inflater.inflate(R.layout.row, parent, false);


        BrssPostItem dvalue = getItem(position);
        TextView pubDate = (TextView) row.findViewById(R.id.pub_date);
        TextView postTitle = (TextView) row.findViewById(R.id.post_title);
        TextView postText = (TextView) row.findViewById(R.id.post_text);
        TextView postAuthor = (TextView) row.findViewById(R.id.post_Author);
        ImageView postImage = (ImageView) row.findViewById(R.id.imagePOST);

       if (dvalue.ImageLink!="NoImage") {
            imageSrc = dvalue.ImageLink;
            Picasso.with(_context).load(imageSrc).into(postImage);
        }
            pubDate.setText(dvalue.Time);
            postTitle.setText(dvalue.Title);
            postText.setText(dvalue.Text);
            postAuthor.setText(dvalue.Author);
            if (dvalue.Readed ==1) {

                postText.setTextColor(Color.GRAY);
                postTitle.setTextColor(Color.GRAY);
                pubDate.setTextColor(Color.GRAY);
                postAuthor.setTextColor(Color.GRAY);

            }
            postText.setTag(position);
            row.setId(position);
        return row;
    }

    @Override
    public int getCount()
    {
        return list.size();
    }
    @Override
    public long getItemId(int position)
    {
        return list.get(position).Id;
    }
    public BrssPostItem getItem(int position)
    {
        return list.get(position);
    }
}
